#!/bin/bash

set -euxo pipefail

# install static linking deps
apt install -y unixodbc unixodbc-dev sqlite3 libsqlite3-dev

dub build --build=release --yes

BUILD_ROOT="pckt-${BUILD_VERSION}-${BUILD_NUMBER}_${BUILD_ARCH}"

rm -rf "${BUILD_ROOT}" && mkdir -p "${BUILD_ROOT}/usr/local/bin"
cp -r "${PWD}/build/deb/DEBIAN" "${BUILD_ROOT}"

install --mode=755 pckt --owner root "${BUILD_ROOT}/usr/local/bin"
render-file "${BUILD_ROOT}/DEBIAN/control"

dpkg-deb --build --root-owner-group "${BUILD_ROOT}"
