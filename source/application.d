module application;
import std.file;
import std.stdio;
import std.format;
import core.thread.osthread;
import std.datetime;
import std.conv;
import std.range;

import ddbc.drivers.sqliteddbc;
import hibernated.core;

import cfg;
import model;
import dblogic;
import cmdparse;


struct Application {

	immutable Config config;
	SessionFactory dbSessionFactory;

	this(immutable Config config, string[] args) {
		this.config = config;
		this.args = args;
		SQLITEDriver driver = new SQLITEDriver();
		string[string] params;
		Dialect dialect = new SQLiteDialect();
		ds = new ConnectionPoolDataSourceImpl(driver, config.storagePath, params);
		EntityMetaData schema = new SchemaInfoImpl!(Pocket,Item);
		dbSessionFactory = new SessionFactoryImpl(schema, dialect, ds);
		dbSessionFactory.getDBMetaData().updateDBSchema(ds.getConnection(), false, true);
	}

	~this() {
		dbSessionFactory.close();
	}

	void run(const ref Request request) {
		auto logic = PocketLogic(dbSessionFactory.openSession(), request.target.pocket);
		switch (request.command) {
			case Command.cleanAll:
				auto removedItems = logic.clean();
				if (removedItems.empty) {
					writeln("There are no expired items");
					return;
				}
				writeln("Removed expired elements:");
				foreach(i; removedItems) {
					writefln("\t%s", i.asString(true));
				}
				break;
			case Command.create:
				auto pocket = logic.create(request.regex);
				writefln(`Pocket %s created`, pocket.name);
				break;
			case Command.push:
				const ulong ttl = request.ttl == 0 ? config.ttl * 3600 * 24 : request.ttl;
				auto item = logic.pushItem(request.value, ttl, request.comment);
				writefln(
					`Added %s to pocket "%s"`,
					item.asString(true), item.pocket.name
				);
				break;
			case Command.front:
				auto items = logic.getItems(request.target.offset, request.target.limit);
				foreach(i, val; items) {
					if (request.verboseOutput) {
						writef(`%d: `, i + request.target.offset);
					}
					writeln(val.asString(request.verboseOutput));
				}
				break;
			case Command.remove:
				Pocket pocket = logic.removePocket();
				writefln(`Deleted pocket %s and all its items`, pocket.name);
				break;
			case Command.update:
				Pocket pocket = logic.updatePocket(request.regex);
				writefln(`Updated pocket %s, regex=%s`, pocket.name, pocket.regex);
				break;
			case Command.show:
				Pocket pocket = logic.getThisPocket();
				writefln(`Pocket: %s`, pocket.asString(true));
				break;
			case Command.search:
				foreach(item; logic.getItemByComment(request.comment)) {
					writeln(item.asString(request.verboseOutput));
				}
				break;
			case Command.empty:
				size_t numberDropped = logic.dropItems();
				if (numberDropped) {
					writefln(`Dropped %d items from pocket "%s"`, numberDropped, request.target.pocket);
				}
				break;
			case Command.renew:
				auto item = logic.renewItem(request.comment, request.target.offset, request.ttl);
				if (item is null) {
					return;
				}			
				writefln(
					`Updated %s in pocket "%s"`,
					item.asString(true), item.pocket.name
				);
				break;
			default:	
				break;
		}
	}
private: 
	DataSource ds;
	string[] args;
}


// pckt <name>  == pckt <name> front
// pckt <name> (create|remove|drop-items)
// pckt <name> push <value> (--ttl <ulong>)
// pckt --clean
// pckt <name> renew
// name - <name:string>^<offset:uint>~<limit:uint> (pckt ip^1 - value in pocket ip after the latest - offset 1)


