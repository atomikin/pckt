module cfg;

import std.file;
import std.path;
import std.exception;
import std.json;
import std.stdio;
import std.conv;
import std.experimental.logger.core;


struct Config {
    size_t ttl;
    string storagePath;
	LogLevel logLevel; 

    JSONValue toJSON() {
        auto result = ["storage_path": storagePath].JSONValue();
        result["ttl"] = ttl;
		result["log_level"] = to!string(logLevel);
        return result;
    }

    static Config load(string path) {
        enforce(path.exists, "Config does not exist");
        JSONValue value = path.readText.parseJSON;
        Config cfg;
        cfg.ttl = value["ttl"].integer;
        cfg.storagePath = value["storage_path"].str;
		cfg.logLevel = to!LogLevel(value["log_level"].str);
        if (!cfg.storagePath.dirName.exists) {
            cfg.storagePath.dirName.mkdirRecurse;
            cfg.storagePath.dirName.setAttributes(octal!700);
        }
        return cfg;
    }

    static void makeDummy(string path) {
        const string base = path.dirName;
        if (!base.exists) {
            base.mkdirRecurse;
        }
        // writeln(1);
        auto f = File(path, "w");
        enforce(f.tryLock());
        // 30 days, 
        f.write(Config(30, "~/.pckt/storage.sqlite".expandTilde, LogLevel.fatal).toJSON().toString());
        path.setAttributes(octal!600);
        f.unlock();
    }
}
