import std.stdio;
import std.file;
import std.path;
import std.experimental.logger.core;

import application;
import cfg;
import model;
import cmdparse;

void main(string[] args)
{
	const string cfgPath = "~/.pckt/app.cfg".expandTilde;
	if (!cfgPath.exists) {
		Config.makeDummy(cfgPath);
	}
	Config config = Config.load(cfgPath);
	globalLogLevel(config.logLevel);

	try {
		auto request = Request.parseArgv(args);
		if (request.help) {
			return;
		}
		Application(config, args).run(request);
	} catch (Exception err) {
		writeln(err.msg);
		import core.stdc.stdlib;
		exit(1);
	}
}
