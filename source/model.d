module model;

import std.datetime.systime;
import std.datetime;
import std.format;
import std.conv;

import hibernated.core;


@Table("items")
class Item {
    @Id
    @Generated
    ulong id;

    ulong ttl;

    @Column("created_at")
    ulong createdAt;
    
    string value;
    Pocket pocket;
    string comment;

    static Item create(Pocket pocket, string value, ulong ttl, string comment = "") {
        Item item = new Item;
        item.pocket = pocket;
        item.value = value;
        item.ttl = ttl;
        item.createdAt = Clock.currTime.toUnixTime;
        item.comment = comment;
        return item;
    }
	string asString(bool verboseOutput = false) {
		if (verboseOutput) {
			return format!`value: "%s", created_at: %d, ttl: %d days, comment: "%s"`(
				value, createdAt, to!size_t(ttl / 24 / 3600), comment
			);
		}
		return value;
	}
}

@Table("pockets")
class Pocket {
    @Id
    @Generated
    ulong id;

    @UniqueKey
    string name;

    @Column("created_at")
    ulong createdAt;

    LazyCollection!Item items;

    @Null
    string regex;

    static Pocket create(string name, string regex = null) {
        auto pocket = new Pocket();
        pocket.name = name;
        pocket.createdAt = Clock.currTime.toUnixTime;
	pocket.regex = regex;

        return pocket;
    }

	string asString(bool verboseOutput = false) {
		if (verboseOutput) {
			return format!`name: "%s", created_at: %s, regex: "%s"`(name, createdAt, regex);
		}
		return name;
	}
}
