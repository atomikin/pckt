module dblogic;

import std.format;
import std.datetime.systime;
import std.experimental.logger;
import std.algorithm;
import std.regex;
import std.range;
import std.stdio;
import std.conv;

import hibernated.core;

import model;

struct PocketLogic {
public:
    this(Session session, string pocketName) {
        this.session = session;
        this.pocketName = pocketName;
    }
    ~this() {
        session.close();
    }

    Pocket create(string regex = null) {
        Pocket pocket = session
            .createQuery("from Pocket where name = :name")
            .setParameter("name", pocketName)
            .uniqueResult!Pocket();
        if (pocket is null) {
            pocket = Pocket.create(pocketName, regex);
            session.save(pocket);
            return pocket;
        }
        return pocket;
    }

    Pocket[] getPockets() {
        return session.createQuery("from Pocket where name like :name order by createdAt desc")
            .setParameter("name", "%" ~ pocketName ~ "%s")
            .list!Pocket();
    }

	Pocket getThisPocket() {
		auto pocket = session.createQuery("from Pocket where name = :name")
			.setParameter("name", pocketName)
			.uniqueResult!Pocket();
		return pocket;

	}

    Item pushItem(string value, ulong ttl, string comment = "") {
        Pocket pocket = getThisPocket(); 
        if (pocket is null) {
            throw new Exception(format!"Pocket %s does not exist"(pocketName));
        }
		if (pocket.regex !is null) {
			if (value.matchFirst(regex(pocket.regex)).empty) {
				throw new Exception(format!"Value '%s' does not match pocket regex '%s'"(value, pocket.regex));
			}
		}
        Item item = Item.create(pocket, value, ttl, comment);
        session.save(item);
        return item;
    }

    Item frontItem() {
        return session.createQuery(
            "from Item where pocket.name = :name order by createdAt desc"
        ).setParameter("name", pocketName)
        .list!Item().front;
    }

    Item[] getItems(size_t offset = 0, int limit = 1) {
        auto result = session
            .createQuery("from Item where pocket.name = :name order by createdAt desc")
            .setParameter("name", pocketName)
            .list!Item();
        limit = limit < 1 ? 1 : limit;
        if (result.empty || result.length <= offset) {
            return [];
        }
        return result[offset..min(offset + limit, result.length)];
    }

    Item renewItem(string comment = "", size_t offset = 0, size_t ttl=0) {
		auto items = getItems(offset);
		if (items.empty) {
			return null;
		}
		auto item = items.front;
        item.createdAt = Clock.currTime.toUnixTime();
		if (comment != "" && item.comment != comment) {
			item.comment = comment;
		}
		if (ttl > 0) {
			item.ttl = ttl;
		}
		item.pocket = getThisPocket();
        session.update(item);
        return item;
    }

	Item[] getItemByComment(string comment) {
		if (comment is null) {
			throw new Exception("Comment is required");
		}
        return session
            .createQuery("from Item where pocket.name = :name and comment like :comment or value like :comment order by createdAt desc")
            .setParameter("comment", "%" ~ comment ~ "%")
            .setParameter("name", pocketName)
            .list!Item();
    }

	Pocket removePocket() {
		auto pocket = getThisPocket();
		if (pocket is null) {
			return pocket;
		}
		dropItems();
		session.remove(pocket);
		return pocket;
	}

	size_t dropItems() {
		auto pocket = getThisPocket();
		size_t deletedItems = pocket.items.length;
		foreach(item; pocket.items) {
			session.remove(item);
		}
		return deletedItems;
	}
	
	Pocket updatePocket(string regex) {
		auto pocket = getThisPocket();
		if (pocket is null) {
			throw new Exception(format!"Pocket %s does not exist"(pocketName));
		}
		pocket.regex = regex;
        session.update(pocket);
		return pocket;
	}
	
	Item[] clean() {
		Item[] items;
		Pocket pocket = getThisPocket();
		if (pocket is null) {
			items = session
				.createQuery(`from Item where :curTime >= (createdAt + ttl) order by createdAt desc`)
				.setParameter("curTime", to!ulong(Clock.currTime.toUnixTime()))
				.list!Item();
		} else {
			items = session
				.createQuery(`from Item where pocket.name == :name and :curTime >= (createdAt + ttl) order by createdAt desc`)
				.setParameter("curTime", to!ulong(Clock.currTime.toUnixTime()))
				.setParameter("name", pocketName)
				.list!Item();
		}
		foreach(i; items) {
			session.remove(i);
		}
		return items;
	}

private:
    Session session;
    const string pocketName;
}
