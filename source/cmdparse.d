module cmdparse;

import std.string;
import std.conv;
import std.format;
import std.range;
import std.getopt;

// !! pckt <name>  == pckt <name> front
// pckt <name> (push|create|remove|empty|clean)
// !! pckt <name> push <value> (--ttl <ulong>)
// pckt --clean
// pckt <name> renew
// name - <name:string>^<offset:uint>~<limit:uint> (pckt ip^1 - value in pocket ip after the latest - offset 1)

enum Command {
    front,
    push,
    create,
    remove,
    empty,
	update,
	show,
    search,
	renew,
    cleanAll,
};

struct Target {

    string pocket;
    uint offset;
    uint limit = 1;

    this(string rawTarget) {
        rawTarget = readParam!uint(rawTarget, "~", &limit);
        rawTarget = readParam!uint(rawTarget, "^", &offset);
        pocket = rawTarget;
    }
private:
    static string readParam(T)(const ref string rawTarget, string separator, T* target) {
        auto lastPart = rawTarget.split(separator)[$-1];
        if (lastPart != rawTarget) {
            *target = to!T(lastPart);
            return rawTarget[0..$-lastPart.length-1];
        }
        return rawTarget;
    } 
}

struct Request {
    Target target;
    Command command;
    string value;
    string comment;
    ulong ttl;
    bool clean;
    bool help;
	bool verboseOutput;
	string regex;

    static Request parseArgv(string[] argv) {
        Request cmd;

        auto helpInformation = getopt(
            argv,
			std.getopt.config.caseSensitive,
            "t|ttl", "TTL of the item", &cmd.ttl,    // numeric
            "clean", "clean whole DB", &cmd.clean,      // string
            "i|verbose-output", "verbose result", &cmd.verboseOutput,      // string
            "c|comment", "comment for push command", &cmd.comment,
			"r|regex", "Regex to restrict pocket content", &cmd.regex,
        );
        if (cmd.clean) {
            cmd.command = Command.cleanAll;
            return cmd;
        }
        if (helpInformation.helpWanted) {
			defaultGetoptPrinter("Pocket usage: \n\t pckt POCKET_NAME [front|push|create|remove|empty|show|search] [ARGS]",
            helpInformation.options);
            cmd.help = true;
            return cmd;
        }
		if (cmd.ttl > 0) {
			cmd.ttl *= 24 * 3600;
		}
        if (cmd.comment is null) {
            cmd.comment = "";
        }
		auto positionals = argv[1..$];
        if (positionals.length == 0) {
            throw new Exception("Pocket name is required");
        }
        cmd.target = Target(positionals.front);
        if (positionals.length >= 2) {
            try {
                cmd.command = to!Command(positionals[1]);
            } catch (Exception err) {
                throw new Exception(
                    format!"Invalid command: %s; should be one of front|push|create|remove|empty"(positionals[$-1])
                );
            }
        }
        if (positionals.length >= 3) {
            cmd.value = positionals[2];
        }
        return cmd;
    }
}


unittest {
    import std.stdio;

    auto target = Target("name");
    assert(target.pocket == "name");
    assert(target.offset == 0);
    assert(target.limit == 1);
    target = Target("name^1");
    writeln(target);
    assert(target.pocket == "name");
    assert(target.offset == 1);
    assert(target.limit == 1);
    target = Target("name^1~4");
    assert(target.pocket == "name");
    assert(target.offset == 1);
    assert(target.limit == 4);
    target = Target("^1~4");
    assert(target.pocket == "");
    assert(target.offset == 1);
    assert(target.limit == 4);
}
