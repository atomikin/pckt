
export BUILD_DIR ?= /tmp/build
export RELEASE ?= minor
export BUILD_VERSION ?= $(shell build-helper build-version --increment -r "${RELEASE}")
export BUILD_NUMBER ?= 1
export BUILD_ARCH ?= amd64
export BRANCH ?= master

all: save-version

save-version: build-deb
	build-helper build-version --save "${BUILD_VERSION}"
	git add version.ini
	git commit -m "Compiled ${BUILD_VERSION}. Release: ${RELEASE}"
	git push origin "${BRANCH}"

build-deb:
	rsync -iah --exclude --exclude '.dub' --exclude 'pckt' "${PWD}/" $(BUILD_DIR)/
	docker run --entrypoint='./build/deb/build.sh' \
		-it -v "$(BUILD_DIR):/dub/build" \
		-e BUILD_NUMBER=${BUILD_NUMBER} \
		-e BUILD_VERSION=${BUILD_VERSION} \
		-e BUILD_ARCH=${BUILD_ARCH} 192.168.1.58:5000/internal/debian/dmd:2.95.0
